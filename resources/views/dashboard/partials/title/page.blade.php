@hasSection('title')
  <div class="page-header">
    <h1>@yield('title')</h1>
  </div>
@elseif(isset($title) && $title != "")
  <div class="page-header">
    <h1>{{ $title }}</h1>
  </div>
@endif