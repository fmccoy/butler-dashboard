<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ route('home') }}">{{ config('app.name') }}</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      
      {{-- Default Nav Links --}}
      <ul class="nav navbar-nav navbar-left">
         @each('dashboard::partials.navbar.item', $navmenu, 'item')
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      @if (Auth::guest())
        @include('dashboard::partials.navbar.loggedout')
      @else
        @include('dashboard::partials.navbar.loggedin')
      @endif
      </ul>

    </div><!-- /.navbar-collapse -->
  </div>
</nav>