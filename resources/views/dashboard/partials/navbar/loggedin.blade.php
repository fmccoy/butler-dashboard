<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
    {{ title_case( Auth::user()->name ) }} <span class="caret"></span>
  </a>

  <ul class="dropdown-menu" role="menu">
    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="{{ route('profile') }}">Profile</a></li>
    <li><a href="{{ route('apps') }}">Applications</a></li>
    <li>
      <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        Logout
      </a>

      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
      </form>
    </li>
  </ul>
</li>