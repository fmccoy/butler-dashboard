@extends('dashboard::master')

@section('navmenu')
  <li><a href="#">Link 1</a></li>
  <li><a href="#">Link 2</a></li>
@endsection

@section('body')
  
  @include('dashboard::partials.navbar')
  
  @yield('layout')

@endsection
