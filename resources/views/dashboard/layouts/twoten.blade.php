@extends('dashboard::templates.app')

@section('layout')

  <div class="container-fluid">
    <div class="row">
      
      <div id="sidebar" class="col-sm-2 sidebar">
        @yield('sidebar')
      </div>
      
      <div id="main" class="col-sm-10 col-sm-offset-2 main">
        @include('dashboard::partials.title.page')
        @yield('content-main')
      </div> 

    </div>
  </div>

@endsection