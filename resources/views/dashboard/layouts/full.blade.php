@extends('dashboard::templates.app')

@section('layout')

  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 main">
        @include('dashboard::partials.title.page')
        @yield('content-main')
      </div> 
    </div>
  </div>

@endsection