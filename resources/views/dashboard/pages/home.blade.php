@extends('dashboard::layouts.full')

@section('content-main')
  <div class="page-header">
    <h1>Dashboard</h1>
  </div>

  @yield('dashboard-content')

@endsection
