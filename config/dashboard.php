<?php

return [
  'route_prefix' => 'dashboard',
  'route_middleware' => ['web', 'auth'],
];