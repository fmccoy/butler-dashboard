<?php

namespace Butler\Dashboard;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard::pages.home');
    }

    public function profile()
    {
        return view('dashboard::pages.profile');
    }

    public function applications()
    {
        return view('dashboard::pages.applications');
    }
}
