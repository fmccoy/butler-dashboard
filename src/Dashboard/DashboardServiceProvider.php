<?php

namespace Butler\Dashboard;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class DashboardServiceProvider extends ServiceProvider
{
  public function boot()
  {
    // Views
    $this->loadViewsFrom(__DIR__.'/../../resources/views/dashboard/', 'dashboard');
    $this->loadViewsFrom(__DIR__.'/../../resources/views/crud/', 'crud');
    
    // Migrations  
    //$this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

    // Publishes
    $this->publishes([

      // Config
        __DIR__.'/../../config/dashboard.php' => config_path('dashboard.php'),
    ]);

    $this->routes();
  }

  public function register()
  {
    // Config
     $this->mergeConfigFrom(
        __DIR__.'/../../config/dashboard.php', 'dashboard'
    );
  }

  public function routes()
  {
     Route::group([
            'prefix' => config('dashboard.route_prefix'),
            'middleware' => config('dashboard.route_middleware'),
            'namespace' => 'Butler\Dashboard',
        ], function ($router) {
            Route::get('/', 'DashboardController@index')->name('dashboard');
            Route::get('profile', 'DashboardController@profile')->name('profile');
            Route::get('apps', 'DashboardController@applications')->name('apps');
        });
  }
}